# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.6](https://git.vptech.eu/vitan/lerna-pipelines/compare/@module-b/api-c@1.0.5...@module-b/api-c@1.0.6) (2019-10-11)

**Note:** Version bump only for package @module-b/api-c





## [1.0.5](https://git.vptech.eu/vitan/lerna-pipelines/compare/@module-b/api-c@1.0.4...@module-b/api-c@1.0.5) (2019-10-11)

**Note:** Version bump only for package @module-b/api-c





## [1.0.4](https://git.vptech.eu/vitan/lerna-pipelines/compare/@module-b/api-c@1.0.3...@module-b/api-c@1.0.4) (2019-10-11)

**Note:** Version bump only for package @module-b/api-c





## [1.0.3](https://git.vptech.eu/vitan/lerna-pipelines/compare/@module-b/api-c@1.0.2...@module-b/api-c@1.0.3) (2019-10-11)

**Note:** Version bump only for package @module-b/api-c





## 1.0.2 (2019-10-11)

**Note:** Version bump only for package @module-b/api-c





## 1.0.1 (2019-10-11)

**Note:** Version bump only for package @module-b/api-c
