# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.2](https://git.vptech.eu/vitan/lerna-pipelines/compare/@module-a/api-a@1.1.1...@module-a/api-a@1.1.2) (2019-10-11)

**Note:** Version bump only for package @module-a/api-a





## [1.1.1](https://git.vptech.eu/vitan/lerna-pipelines/compare/@module-a/api-a@1.1.0...@module-a/api-a@1.1.1) (2019-10-11)

**Note:** Version bump only for package @module-a/api-a





# [1.1.0](https://git.vptech.eu/vitan/lerna-pipelines/compare/@module-a/api-a@1.0.3...@module-a/api-a@1.1.0) (2019-10-11)


### Features

* hello world ([6cc59ac](https://git.vptech.eu/vitan/lerna-pipelines/commits/6cc59ac))





## [1.0.3](https://git.vptech.eu/vitan/lerna-pipelines/compare/@module-a/api-a@1.0.2...@module-a/api-a@1.0.3) (2019-10-11)

**Note:** Version bump only for package @module-a/api-a





## 1.0.2 (2019-10-11)

**Note:** Version bump only for package @module-a/api-a





## 1.0.1 (2019-10-11)

**Note:** Version bump only for package @module-a/api-a
